package com.rishi.test;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import com.rishi.app.util.BuildInsurance;

import junit.framework.Assert;


public class TestApp {
	
	@Test
	public void testQuote(String args){
		String result = "{\"totalQuote\":5000.24,\"age\":34,\"name\":\"Norman Gomes\",\"gender\":\"1\",\"health\":{\"bloodPressure\":false,\"overweight\":true,\"hypertension\":false},\"habits\":{\"dailyExcercise\":\"good\",\"alcohol\":\"bad\",\"smoking\":false}}";
		JSONObject mainObj = new JSONObject();
		try {
			mainObj = new JSONObject(result);
			int age = mainObj.getInt("age");
			int gender = mainObj.getInt("gender");
			JSONObject habits = mainObj.getJSONObject("habits");
			JSONObject conditions = mainObj.getJSONObject("health");

			double totalQuote = new BuildInsurance()
					.ageCalc(age)
					.genderCalc(gender)
					.habitCalc(habits)
					.conditionCalc(conditions)
					.getTotalQuote();
			System.out.println("## "+totalQuote);
			
			Assert.assertEquals(totalQuote, 5856);


		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}

}
