package com.rishi.app.util;

public class PercentUtil {
	
	public static double getPercentOf(double number, float percent){
		double percentage = 0;
		percentage = (percent/100)*number;
		return percentage;
	}

}
