package com.rishi.app.util;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

public class BuildInsurance {

	private double totalQuote = CONST.BASE_AMOUNT;

	public double getTotalQuote() {
		return totalQuote;
	}

	public BuildInsurance ageCalc(int age) {
		if (age >= 18) {
			totalQuote += PercentUtil.getPercentOf(totalQuote, 10);
			System.out.println(" 1a totalQuote :: " + totalQuote);

		}
		if (age >= 25) {
			totalQuote += PercentUtil.getPercentOf(totalQuote, 10);
			System.out.println(" 1b totalQuote :: " + totalQuote);

		}
		if (age >= 30) {
			totalQuote += PercentUtil.getPercentOf(totalQuote, 10);
			System.out.println(" 1c totalQuote :: " + totalQuote);

		}
		if (age >= 35) {
			totalQuote += PercentUtil.getPercentOf(totalQuote, 10);
			System.out.println(" 1d totalQuote :: " + totalQuote);

		}
		if (age >= 40) {
			int extraAge = age - 40;
			for (int i = 0; i <= extraAge / 5; i++)
				totalQuote += PercentUtil.getPercentOf(totalQuote, 20);
			System.out.println(" 1e totalQuote :: " + totalQuote);

		}

		return this;
	}

	public BuildInsurance genderCalc(int gender) {
		if (gender == 1) {
			totalQuote += PercentUtil.getPercentOf(totalQuote, 2);
		}
		System.out.println(" 2 totalQuote :: " + totalQuote);

		return this;
	}

	public BuildInsurance habitCalc(JSONObject jObj) {
		Iterator<?> keys = jObj.keys();
		try {
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (jObj.get(key) instanceof String) {
					String habit = (String) jObj.get(key);
					System.out.println(" Habit :: " + habit);
					if (habit.equalsIgnoreCase("bad")) {
						totalQuote += PercentUtil.getPercentOf(CONST.BASE_AMOUNT, 3);
						System.out.println(" 3a totalQuote :: " + totalQuote);

					} else if (habit.equalsIgnoreCase("good")) {
						totalQuote -= PercentUtil.getPercentOf(CONST.BASE_AMOUNT, 3);
						System.out.println(" 3b totalQuote :: " + totalQuote);

					}

				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return this;
	}

	public BuildInsurance conditionCalc(JSONObject jObj) {
		Iterator<?> keys = jObj.keys();
		try {
			while (keys.hasNext()) {
				String key = (String) keys.next();
				System.out.println(" Condition :: " + key);

				if (jObj.get(key) instanceof Boolean) {
					if (jObj.getBoolean(key)) {
						totalQuote += PercentUtil.getPercentOf(totalQuote, 1);
						System.out.println(" 4 totalQuote :: " + totalQuote);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return this;
	}

}
