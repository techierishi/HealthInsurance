package com.rishi.app.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rishi.app.model.User;
import com.rishi.app.util.BuildInsurance;

@Controller
@RequestMapping("/rest")
public class InsRestController {

	@GetMapping("/users")
	public ResponseEntity<List<User>> getAllUsers() {
		List<User> list = new ArrayList<User>();
		list.add(new User(1, "Rishi"));
		list.add(new User(2, "Sinki"));
		return new ResponseEntity<List<User>>(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/calculate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void getCalculation(HttpEntity<String> httpEntity, HttpServletRequest request,
			HttpServletResponse response) {
		String result = httpEntity.getBody();
		JSONObject mainObj = new JSONObject();
		try {
			mainObj = new JSONObject(result);
			int age = mainObj.getInt("age");
			int gender = mainObj.getInt("gender");
			JSONObject habits = mainObj.getJSONObject("habits");
			JSONObject conditions = mainObj.getJSONObject("health");

			double totalQuote = new BuildInsurance()
					.ageCalc(age)
					.genderCalc(gender)
					.conditionCalc(conditions)
					.habitCalc(habits)
					.getTotalQuote();
			mainObj.put("totalQuote", totalQuote);

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		response.setContentType("application/json");

		try {
			PrintWriter out = response.getWriter();
			out.write(mainObj.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}