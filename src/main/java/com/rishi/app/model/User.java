package com.rishi.app.model;

public class User{
	
	private int id;
	private String name;
	
	public User(int _id,String _name){
		id=_id;
		name=_name;
	}
	
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		
		return name;
	}
	
	
}