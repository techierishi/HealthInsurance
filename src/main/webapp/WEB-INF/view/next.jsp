<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>

<title> Premium Quote </title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>
<style>
.bd-example {
    position: relative;
    padding: 1rem;
    margin: 1rem -1rem;
    border: solid #f7f7f9;
    border-width: .2rem;
}
</style>
</head>

<body ng-app="demoApp" ng-controller="demoController">

<div class="container">
      <div class="row">
	  <div class="col-12 col-md-3 push-md-9 bd-sidebar">
	  </div>
        <div class="col-12 col-md-9 pull-md-3 bd-content">
		<div class="bd-example" data-example-id="">
<form>

  <div class="form-group">
    <label for="exampleName">Name</label>
    <input type="text" class="form-control" ng-model="formData.name" id="exampleName" placeholder="Enter name">
  </div>
  <div class="form-group">
    <label for="exampleAge">Age</label>
	  <input class="form-control" type="number" ng-model="formData.age" placeholder="Enter age" id="exampleAge">

  </div> 
  
  
  <div class="form-group">
    <label for="exampleGender">Gender</label>
    <select class="form-control" id="exampleGender" ng-model="formData.gender">
      <option>Male</option>
      <option>Female</option>
      <option>Other</option>
    </select>
  </div>
  <fieldset class="form-group">
    <legend>Current Health</legend>
    <div class="form-check">
     <label class="form-check-label">
      <input type="checkbox" class="form-check-input" ng-model="formData.health.hypertension">
      Hypertenstion
    </label>
	<label class="form-check-label">
      <input type="checkbox" class="form-check-input" ng-model="formData.health.bloodPressure">
      Blood pressure
    </label>
	<label class="form-check-label">
      <input type="checkbox" class="form-check-input" ng-model="formData.health.bloodSuger">
      Blood suger
    </label>
	<label class="form-check-label">
      <input type="checkbox" class="form-check-input" ng-model="formData.health.overweight">
      Overweight
    </label>
    </div>
	
   
  </fieldset>
   <fieldset class="form-group">
    <legend>Habits</legend>
    <div class="form-check">
     <label class="form-check-label">
      <input type="checkbox" class="form-check-input" ng-model="formData.habits.smoking">
      Smoking
    </label>
	<label class="form-check-label">
      <input type="checkbox" class="form-check-input" ng-model="formData.habits.alcohol">
      Alocohol
    </label>
		<label class="form-check-label">
      <input type="checkbox" class="form-check-input" ng-model="formData.habits.dailyExcercise">
      Daily excercise
    </label>
		<label class="form-check-label">
      <input type="checkbox" class="form-check-input" ng-model="formData.habits.drugs">
      Drugs
    </label>
    </div>
	
   
  </fieldset>
  <button type="submit" class="btn btn-primary" ng-click="serialize($event)">Submit</button>
</form>
</div>
</div>
</div>
</div>

<script>

var app = angular.module('demoApp', []);

app.controller('demoController', function($scope) {
  $scope.formData = {}
  $scope.serialize = function($event){
    console.log($scope.formData)
    $event.preventDefault()
  }
});
</script>
</body>