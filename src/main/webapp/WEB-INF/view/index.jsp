<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>

<title>Premium Quote</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>
<style>
.bd-example {
	position: relative;
	padding: 1rem;
	margin: 1rem -1rem;
	border: solid #f7f7f9;
	border-width: .2rem;
}
</style>
</head>

<body ng-app="demoApp" ng-controller="demoController">

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-3 push-md-9 bd-sidebar"></div>
			<div class="col-12 col-md-9 pull-md-3 bd-content">
				<div class="bd-example" data-example-id="">

					<form>

						<div class="form-group">
							<label for="exampleName">Name</label> <input type="text"
								class="form-control" ng-model="formData.name" id="exampleName"
								placeholder="Enter name">
						</div>
						<div class="form-group">
							<label for="exampleAge">Age</label> <input class="form-control"
								type="number" ng-model="formData.age" placeholder="Enter age"
								id="exampleAge">

						</div>


						<div class="form-group">
							<label for="exampleGender">Gender</label> <select
								class="form-control" id="exampleGender"
								ng-model="formData.gender">
								<option value="1">Male</option>
								<option value="2">Female</option>
								<option value="3">Other</option>
							</select>
						</div>
						<fieldset class="form-group">
							<legend>Current Health</legend>
							<div class="form-check">
								<label class="form-check-label"> <input type="checkbox"
									class="form-check-input"
									ng-model="formData.health.hypertension"> Hypertenstion
								</label> <label class="form-check-label"> <input type="checkbox"
									class="form-check-input"
									ng-model="formData.health.bloodPressure"> Blood
									pressure
								</label> <label class="form-check-label"> <input type="checkbox"
									class="form-check-input" ng-model="formData.health.bloodSuger">
									Blood suger
								</label> <label class="form-check-label"> <input type="checkbox"
									class="form-check-input" ng-model="formData.health.overweight">
									Overweight
								</label>
							</div>


						</fieldset>
						<fieldset class="form-group">
							<legend>Habits</legend>
							<div class="form-check">
								<label class="form-check-label"> <input type="checkbox"
									class="form-check-input" ng-model="formData.habits.smoking"
									ng-init="formData.habits.smoking='none'" ng-true-value="'bad'">
									Smoking
								</label> <label class="form-check-label"> <input type="checkbox"
									class="form-check-input" ng-model="formData.habits.alcohol"
									ng-init="formData.habits.alcohol='none'" ng-true-value="'bad'">
									Alocohol
								</label> <label class="form-check-label"> <input type="checkbox"
									class="form-check-input"
									ng-model="formData.habits.dailyExcercise"
									ng-init="formData.habits.dailyExcercise='none'" ng-true-value="'good'">
									Daily excercise
								</label> <label class="form-check-label"> <input type="checkbox"
									class="form-check-input" ng-model="formData.habits.drugs"
									ng-init="formData.habits.drugs='none'" ng-true-value="'bad'">
									Drugs
								</label>
							</div>


						</fieldset>
						<div class="row">
							<div class="col-12 col-md-3">
								<button type="submit" class="btn btn-primary"
									ng-click="serialize($event)">Get Quote</button>
							</div>
							<div class="col-12 col-md-9">
								<div class="alert alert-success" ng-show="showMessage"
									style="height: 40px; padding-top: 8px; width: 100%;">
									<button type="button" class="close" data-dismiss="alert"
										aria-hidden="true">�</button>
									<span class="glyphicon glyphicon-ok"></span> <span
										id="quoteAmt">{{quoteAmount}}</span>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script>
		var app = angular.module('demoApp', []);

		app
				.controller(
						'demoController',
						function($scope, $http) {

							$scope.showMessage = false;

							$scope.formData = {}
							$scope.serialize = function($event) {
								console.log($scope.formData);

								$scope.sendData = function() {
									var data = $scope.formData;

									$http(
											{
												method : "POST",
												url : "${pageContext.request.contextPath}/rest/calculate",
												data : data,
											})
											.then(
													function mySuccess(response) {
														$scope.showMessage = true;
														$scope.quoteAmount = "Total Quote = "
																+ (response.data.totalQuote
																		.toFixed(2));

														console.log(response);
													},
													function myError(response) {
														console.log(response);

													});
								};

								$scope.sendData();
								$event.preventDefault()
							}
						});
	</script>
</body>